#!/usr/bin/env sh
set -e
set -x

CONTAINER=${CI_REGISTRY_IMAGE}:pipeline-${ARCH}-${RUST_IMAGE}-${TAURI_CLI_VERSION}-${JDK_VERSION}-${NODE_VERSION}-${NDK_VERSION}-${ANDROID_NATIVE_API_LEVEL}
podman login "${CI_REGISTRY}" -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"

if ! test `podman pull ${CONTAINER}`;
then
  wget 'https://gitlab.com/loikki/pipelines/-/raw/main/containers/Dockerfile?ref_type=heads' -O Dockerfile
  sed -i "s/RUST_IMAGE/${RUST_IMAGE}/g" Dockerfile
  sed -i "s/JDK_VERSION/${JDK_VERSION}/g" Dockerfile
  sed -i "s/NODE_VERSION/${NODE_VERSION}/g" Dockerfile
  sed -i "s/NDK_VERSION/${NDK_VERSION}/g" Dockerfile
  sed -i "s/ANDROID_NATIVE_API_LEVEL/${ANDROID_NATIVE_API_LEVEL}/g" Dockerfile
  sed -i "s/TAURI_CLI_VERSION/${TAURI_CLI_VERSION}/g" Dockerfile
  cat Dockerfile
  podman build . -t ${CONTAINER}
  podman push ${CONTAINER}
fi
